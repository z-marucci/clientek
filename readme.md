# Clientec
## Description
Clientec is a Customer Resource Manager in its early stages of development.
### Features
- Create new clients
- Edit existing and update clients
- Deleting and mass deleting clients
- Sorting and Filtering your clients in various ways
#### Technologies and Tools
- Laravel 5.2
- Gulp
- Bootstrap Sass
- Jquery
- Bower
- Npm
- Ajax
- Sass
- Font Awesome

##### Installation Instructions
- Sudo composer Install
- Sudo npm install
- bower install
- gulp
- php artisan migrate --seed
