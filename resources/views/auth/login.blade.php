@include('partials/header')
<body class="login--body">
    <div class="container-fluid">
        <div class="container">
            <div class="jumbotron login--jumbotron">
                <h1>Manage your Clients now!</h1>

                <form method="POST" action="{{ url('/login') }}">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        Email
                        <input required class="form-control" type="email" name="email" value="{{ old('email') }}">
                    </div>

                    <div class="form-group">
                        Password
                        <input required class="form-control" type="password" name="password" id="password">
                    </div>
                    
                    <hr>

                    <div class="form-group">
                        <input class="" type="checkbox" name="remember"> Remember Me
                    </div>

                    <div class="form-group">
                        <button class="btn btn-success btn-lg btn-block"type="submit">Login</button>
                    </div>

                    <hr>
                </form>
            </div>

            <div class="layout--copyright">
                @include('partials.copyright')
            </div>

        </div>
    </div>
@include('partials/footer')