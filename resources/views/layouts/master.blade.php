@include('partials/header')
<body id="app-layout">
    @include('partials/nav')

    @yield('content')

@include('partials/footer')

    
