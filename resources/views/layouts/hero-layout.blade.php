@include('partials.header')
<body class="body--main">
    <nav class="hero--nav navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Clientec</a>
        </div>
        
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-right">
                
                <li class="">
                    <a href="/clients">Clients</a>
                </li>

                @if(Auth::user())
                    <li class="">
                        <a>Hello, {{Auth::user()->name}}</a>
                    </li>

                    <li class="">
                        <a href="/logout">Logout</a>
                    </li>
                @else
                    <li class="">
                        <a href="/login">Login</a>
                    </li>
                @endif
          </ul>
        </div>
      </div>
    </nav>
    @yield('content')
    <div class="hero-layout--footer">
        @include('partials.footer')
    </div>

    <div class="hero-layout--copyright">
        @include('partials.copyright')
    </div>
    @yield('scripts.footer')
    
    </body>
</html>