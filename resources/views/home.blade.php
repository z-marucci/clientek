@include('partials/header')
<body class="home--body">
    <div class="container-fluid">
        <div class="container">
            <div class="jumbotron home--jumbotron">
                <h1>Welcome to Clientec</h1>


                <h2>What is Clientec?</h2>

                <hr>
                
                <h3>Clientec is a Customer Resource manager in its early stages of development</h3>
                <h3>How can Clientec Help you?</h3>

                <h3> <i class="fa fa-users"></i> Create new clients</h3>
                <h3> <i class="fa fa-pencil"></i> Edit Existing and Update clients</h3>
                <h3> <i class="fa fa-trash"></i> Deleting and mass deleting clients</h3>
                <h3> <i class="fa fa-sort"></i> Sorting and filtering your clients in numerous ways</h3>

                @if (Auth::user())
                    <a href="/clients"><button class="btn-block btn-success btn-lg">Let's Get started</button></a>
                @else 
                    <a href="/login"><button class="btn-block btn-success btn-lg">Let's Get started</button></a>
                @endif

            </div>

            <div class="layout--copyright">
                @include('partials.copyright')
            </div>

        </div>
    </div>
@include('partials/footer')