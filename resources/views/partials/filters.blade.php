{{Form::open(
	array (
		'action' => 'ClientsController@index',
		'method' => 'get',
		'accept-charset' => 'UTF-8',
		'enctype' => 'multipart/form-data',
		'id' => 'form_team_building'
	)
)}}

	<div class="col-md-4">
		<div class="form-group">
			<label for="id">Client Id</label>
			<input type="text" name="id" class="form-control" placeholder="" value="{{old('id')}}">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
			<label for="personal_id">Personal Id</label>
			<input type="text" name="personal_id" class="form-control" value="{{old('personal_id')}}">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
			<label for="email">Email</label>
			<input type="text" name="email" class="form-control" value="{{old('email')}}">
		</div>
	</div>

	
	<div class="col-md-4">
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" name="name" class="form-control" value="{{old('name')}}">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
		<label for="address">Address</label>
		<input type="text" name="address" class="form-control" value="{{old('address')}}">
	</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
			<label for="phone_number">Phone Number</label>
			<input type="text" name="phone_number" class="form-control" value="{{old('phone_number')}}">
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-group">
			<label for="company">Company</label>
			<input type="text" name="company" class="form-control" value="{{old('company')}}">
		</div>	
	</div>

	<div class="col-md-4">
		<div class="form-group">
			<label for="name">Created at - range 1</label>
			<input type="text" name="dob_1" class="form-control datepicker" value="{{old('dob_1')}}">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group">
			<label for="name">Created at - range 2</label>
			<input type="text" name="dob_2" class="form-control datepicker" value="{{old('dob_2')}}">
		</div>
	</div>

	<button type="submit" class="btn btn-lg btn-success index-btn">
		Submit
	</button>

{{Form::close()}}