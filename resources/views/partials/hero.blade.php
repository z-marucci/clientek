<div id="myCarousel" class="carousel slide hero--slider" data-ride="carousel">
<!-- Wrapper for slides -->
    <div class="carousel-caption">
      <h1>{{$hero_heading}}</h1>
    </div>
<div class="hero--slider-overlay"></div>
<div class="carousel-inner">
    <div class="item active">
    	@if ($has_image)
        	<img src="{{$path}}" alt="Chania">
        @else
        	<img src="../../site-images/{{$image}}" alt="Chania">
        @endif
    </div>
    </div>
</div>
