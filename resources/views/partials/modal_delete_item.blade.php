<div class="modal fade" role="dialog" id="modal_borrar_item">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>

                <h4 class="modal-title">Deleting client: </h4>
            </div>

            <div class="modal-body"> 
                    <h3 class="modal--message"><strong>Deleting Current Client</strong></h3>
                    <hr>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>