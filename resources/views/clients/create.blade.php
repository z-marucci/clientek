@inject('countries', 'App\Utilities\Country')
@extends('layouts.hero-layout')
	
	@section('content')
		@include('partials.hero', 
			[
				'has_image' => false,
				'hero_heading' => 'Create a new client',
				'image' => 'create.jpg'
			])
	<div class="container clients--container-create">
		<div class="row clients--main-create">

			<div class="">
				<div class="col-md-12">
					<h1>
						Create Client
					</h1>

					<hr>
					@if(count($errors) > 0)
						<div class="alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</div>
				

				{{Form::open(
					array (
						'action' => 'ClientsController@store',
						'method' => 'post',
						'accept-charset' => 'UTF-8',
						'enctype' => 'multipart/form-data',
						'id' => 'form_team_building'
					)
				)}}
					<div class="col-md-6">
						<div class="form-group">
							{{Form::label('name *')}}
							{{Form::text('name', old('name'), ['class' =>'form-control'])}}
						</div>

						<div class="form-group">
							{{Form::label('personal_id *')}}
							{{Form::text('personal_id', old('personal_id'), ['class' => 'form-control'])}}
						</div>

						<div class="form-group">
							{{Form::label('email *')}}
							{{Form::text('email', old('email'), ['class' => 'form-control'])}}
						</div>

						<div class="form-group">
							<label for="country">Country *</label>
							<select name="country"  id="country" value="{{old('country')}}" class="form-control">
								@foreach($countries::all() as $country => $code)
									<option value="{{$code}}">{{$country}}</option>
								@endforeach
							</select>
						</div>

					</div>

					<div class="col-md-6">
						<div class="form-group">
							{{Form::label('Phone Number')}}
							{{Form::text('phone_number', old('phone_number'), ['class' => 'form-control'])}}
						</div>

						<div class="form-group">
							{{Form::label('address')}}
							{{Form::textarea('address', old('address'), ['class' => 'form-control', 'rows' => '4'])}}
						</div>

						<div class="form-group">
							{{Form::label('company')}}
							{{Form::text('company', old('company'), ['class' => 'form-control'])}}
						</div>



					</div>

					<div class="col-md-12">
						{{Form::button('Create', array('class' => 'btn btn-success btn-lg', 'type' => 'submit'))}}
					</div>
					
				{{Form::close()}}
				
			</div>
		</div>
	</div>
@section('scripts')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	 <script>
	 $(function() {
	   $( "#datepicker" ).datepicker();
	 });
	 </script>
@endsection
@endsection
