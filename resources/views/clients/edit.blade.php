@inject('countries', 'App\Utilities\Country')

@extends('layouts.hero-layout')
	
	@section('content')
		@include('partials.hero', 
			[
				'hero_heading' => trim_string($client->name),
				'has_image' => false,
				'image' => 'edit.jpg',
			])
	<div class="container clients--container-edit">
		<div class="row clients--main-edit">

			<div class="">
				<div class="col-md-12">
					<h1>
						Edit Client
					</h1>

					<hr>
					@if(count($errors) > 0)
						<div class="alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
				</div>
				

				<form action="/clients/{{$client->id}}" method="POST">
					{{csrf_field()}}
					{{ method_field('PATCH') }}
					
					<div class="col-md-6">
						<div class="form-group">
						{{ Form::label('name', 'Name *') }}
						{{Form::text('name', $client->name, ['class' => 'form-control'])}}
					</div>

						<div class="form-group">
							{{Form::label('personal_id')}}
							{{Form::text('personal_id', $client->personal_id, ['class' => 'form-control'])}}
						</div>

						<div class="form-group">
							{{Form::label('email')}}
							{{Form::text('email', $client->email, ['class' => 'form-control'])}}
						</div>
					
						<div class="form-group">
							{{Form::label('Phone Number')}}
							{{Form::text('phone_number', $client->phone_number, ['class' => 'form-control'])}}
						</div>

					</div>

					<div class="col-md-6">
						<div class="form-group">
							{{Form::label('address')}}
							{{Form::textarea('address', $client->address,  ['class' => 'form-control', 'rows' => '4'])}}
						</div>

						<div class="form-group">
							{{Form::label('company')}}
							{{Form::text('company', $client->company, ['class' => 'form-control'])}}
						</div>


						<div class="form-group">
							<label for="country">Country *</label>
							<select name="country"  id="country" value="{{$client->country}}" class="form-control">
								@foreach($countries::all() as $country => $code)
									<option value="{{$code}}">{{$country}}</option>
								@endforeach
							</select>
						</div>

					</div>

					<div class="col-md-12">
						{{Form::button('Update', array('class' => 'btn btn-success btn-lg', 'type' => 'submit'))}}
					</div>
					
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
		$(function() {
			$( ".datepicker" ).datepicker();
		});
	</script>
@endsection