@extends('layouts.hero-layout')

@section('content')

	@include('partials.hero', 
		[
			'hero_heading' => 'Clients are waiting, get started!',
			'image' => 'index.jpg',
			'has_image' => false
		])
	<div class="container clients--container">
		<div class="row clients--main-index">
			<div class="col-md-12">
				<h1>
					Clients
				</h1>
				
				<hr>
				<h2>Actions</h2>

				<a href="{{action('ClientsController@create')}}">
					<button class="btn btn-info btn-lg index-btn">New Client</button>
				</a>

				{{-- <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modal_borrar_item"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</button> --}}

				<button class="btn btn-danger btn-lg delete_all index-btn" data-url="{{ url('clients/deleteallclients') }}">Delete All Selected</button>
				
				<hr>

				<button id="index--filters" class="btn btn-primary btn-lg index-btn">Filters</button>
				<div id="index--filters-group" style="display:none">
					
					@include('partials.filters')

					{{Form::open(
						array (
							'action' => 'ClientsController@index',
							'method' => 'get',
							'accept-charset' => 'UTF-8',
							'enctype' => 'multipart/form-data',
							'id' => 'form_team_building'
						)
					)}}

						<button type="submit" class="btn btn-lg btn-warning">
							Reset
						</button>
					{{Form::close()}}

				</div>
				
				<div class="clients--main table-responsive">
					@if (count($clients) >= 1)
						<table class="table table-striped table-hover clients--table">
							<thead>
								<tr>
									<th class="text-center">
										<p>Action </p>
										<input type="checkbox" name="input_client_top" id="master">
										
									</th>
									<th class="text-center">
										Client Id
									</th>

									<th class="text-center">
										Personal Id
									</th>

									<th class="text-center">
										Name
									</th>

									<th class="text-center">
										Email
									</th>
									<th class="text-center">
										Address
									</th>

									<th class="text-center">
										Phone Number
									</th>

									<th class="text-center">
										Created At
									</th>

									<th class="text-center">
										Country
									</th>

									<th class="text-center">
										Company
									</th>

									<th class="text-center">	
										Actions
									</th>
								</tr>
							</thead>
							<tbody>

									@foreach ($clients as $client)
										<tr>
											<td class="text-center">
												<input type="checkbox" class="sub_chk" data-id="{{$client->id}}">
											</td>
											<td class="text-center">
												{{$client->id}}
											</td>

											<td class="text-center">
												<a href="{{action('ClientsController@show', ['id' => $client->id])}}">
													{{$client->personal_id}}
												</a>
											</td>

											<td class="text-center">
												{{$client->name}}
											</td>

											<td class="text-center">
												{{$client->email}}
											</td>

											<td class="text-center">
												{{trim_string($client->address)}}
											</td>

											<td class="text-center">
												{{$client->phone_number}}
											</td>

											<td class="text-center">
												{{$client->created_at}}
											</td>

											<td class="text-center">
												{{get_country_name($client->country)}}
											</td>

											<td class="text-center">
												{{$client->company}}
											</td>

											<td>
												<a class="btn btn-info" href="{{action('ClientsController@edit', ['id' => $client->id])}}">
													<i class="fa fa-pencil"></i>
												</a>
												
												<form action = "/clients/{{$client->id}}"" method = "POST">
													{{csrf_field()}}
													{{method_field('delete')}}

													<button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i></button>
												</form>
											</td>
										</tr>
									@endforeach
								@else
									<h2>No Clients found</h2>
									<br>
								@endif
							</tbody>
						</table>
					@if ($links)
						{{ $clients->appends([
							'email' => request('email'), 
							'personal_id' => request('personal_id'),
							'company' => request('company'),
							'name' => request('name'),
							'dob_1' => request('dob_1'),
							'dob_2' => request('dob_2'),
							'id' => request('id'),
							'address' => request('address'),
							'email' => request('email')])->render() 
						}}
					@endif
				</div>
			</div>
		</div>
	</div>
@endsection

@include('partials.modal_delete_item')

@section('scripts')
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
		$(function() {
			$( ".datepicker" ).datepicker();
		});
	</script>
@endsection