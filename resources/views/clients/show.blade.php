@extends('layouts.hero-layout')
	
	@section('content')
		@include('partials.hero', 
			[
				'hero_heading' => trim_string($client->name),
				'has_image' => false,
				'image' => 'about.jpg',
			])
		<div class="container client--container-show">
		<div class="row">
			<div class="col-md-12">
				<div class="clients--main-show">

					<div class="jumbotron">
							<a class="pull-right" href="{{action('ClientsController@edit', $client->id)}}"><i class="fa fa-pencil fa-2x"></i></a>
						<div class="container">
							<div class="col-md-4">
								<img class="img-responsive" src="/site-images/missing_image.png" alt="">
								<p class="text-center"><strong>Client id: {{$client->id}}</strong></p>
							</div>

							<div class="col-md-8">
								<h2><strong>{{$client->name}} - {{$client->personal_id}}</strong></h2>
								<h2>Personal info:</h3>
									<p><strong>Email:</strong> {{$client->email}}</p>
									<p><strong>Address:</strong> {{$client->address}}</p>
									<p><strong>Company:</strong> {{$client->company}}</p>
									<p><strong>Country:</strong> {{get_country_name($client->country)}}</p>
									<p><strong>Phone Number:</strong> {{$client->phone_number}}</p>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h2>Other Details</h2>
						</div>

						<div class="panel-body">
							<div class="col-md-6">
								<p>	
									<strong>Created at: </strong>{{$client->created_at}}
								</p>

								<p>	
									<strong>Updated at: </strong>{{$client->updated_at}}
								</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
@endsection