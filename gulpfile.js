var elixir = require('laravel-elixir');
var gulp = require('gulp');
var imagemin = require('gulp-imagemin');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

 gulp.task('imgcrush', () =>
	gulp.src('resources/img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('public/site-images'))
 );

elixir(function(mix) {
    mix.sass('app.scss');
    mix.scripts('modal_delete.js');
    mix.scripts('filters.js');
    mix.task('imgcrush');
});
