$(document).ready(function () {
    
    $("#index--filters").click(function () {
        $("#index--filters-group").toggle();
    });

    $( ".datepicker" ).datepicker();


    $('#master').on('click', function(e) {
     if($(this).is(':checked',true))  
     {
        $(".sub_chk").prop('checked', true);  
     } else {  
        $(".sub_chk").prop('checked',false);  
     }  
    });

    $('.delete_all').on('click', function(e) {

        var allVals = [];  
        $(".sub_chk:checked").each(function() {  
            allVals.push($(this).attr('data-id'));
        });  

        if(allVals.length <=0)  
        {  
            $('#modal_borrar_item').modal('show');
            $('#modal_borrar_item .modal--message').text('Please select a client');
        }  else {  

            var check = confirm("Are you sure you want to delete this row?");  
            if(check == true){  

                var join_selected_values = allVals.join(","); 

                $.ajax({
                    url: $(this).data('url'),
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: 'ids='+join_selected_values,
                    success: function (data) {
                        if (data['success']) {
                            $(".sub_chk:checked").each(function() {  
                                $(this).parents("tr").remove();
                            });
                            $('#modal_borrar_item').modal('show');
                            location.reload();

                        } else if (data['error']) {
                            alert(data['error']);
                        } else {
                            alert('Whoops Something went wrong!!');
                        }
                    },
                    error: function (data) {
                        alert(data.responseText);
                    }
                });

              $.each(allVals, function( index, value ) {
                  $('table tr').filter("[data-row-id='" + value + "']").remove();
              });
            }  
        }  
    });

    

    $(document).on('confirm', function (e) {
        var ele = e.target;
        e.preventDefault();

        $.ajax({
            url: ele.href,
            type: 'DELETE',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (data) {
                if (data['success']) {
                    //$("#" + data['tr']).slideUp("slow");
                    $('#modal_borrar_item').modal('show');
                } else if (data['error']) {
                    alert(data['error']);
                } else {
                    alert('Whoops Something went wrong!!');
                }
            },
            error: function (data) {
                alert(data.responseText);
            }
        });

        return false;
    });
});

//# sourceMappingURL=modal_delete.js.map

//# sourceMappingURL=modal_delete.js.map
