<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 200)->nullable();
            $table->string('email', 200)->nullable();
            $table->string('personal_id', 50)->nullable();
            $table->string('country')->nullable();
            $table->text('address', 2000)->nullable();
            $table->string('phone_number', 50)->nullable();
            $table->integer('user_id')->nullable()->unsigned();
            $table->string('company', 200)->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
