<?php

use App\User;

class UsersTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        $user = new User;
        $user->name = 'Administrator';
        $user->email = 'admin@clientec.com';
        $user->password = bcrypt('secret');
        $user->save();

        $user = new User;
        $user->name = 'Guest';
        $user->email = 'guest@gmail.com';
        $user->password = bcrypt('guest123');
        $user->save();
    }
}
