<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Requests\ClientsRequest;
use App\Client;
use Illuminate\Support\Facades\Auth;

class ClientsController extends Controller
{

    public function __construct() {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $links = true;
        if ($request->get('id') && $request->get('id') != "") {
            $id = $request->get('id');
            $clients = Client::where('id', $id)
             ->paginate(10);
        } elseif ($request->get('personal_id') && $request->get('personal_id') != "") {
            $personal_id = $request->get('personal_id');
            $clients = Client::where('personal_id', 'like', "%$personal_id%")
             ->paginate(10);
        } elseif ($request->get('company') && $request->get('company') != "") {
            $company = $request->get('company');
            $clients = Client::where('company', 'like', "%$company%")
             ->paginate(10);
        } elseif ($request->get('email') && $request->get('email') != "") {
            $email = $request->get('email');
            $clients = Client::where('email', 'like', "%$email%")
             ->paginate(10);
        } elseif ($request->get('name') && $request->get('name') != "") {
            $name = $request->get('name');
            $clients = Client::where('name', 'like', "%$name%")
             ->paginate(10);
        } elseif ($request->get('address') && $request->get('address') != "") {
            $address = $request->get('address');
            $clients = Client::where('address', 'like', "%$address%")
             ->paginate(10);
        } elseif ($request->get('phone_number') && $request->get('phone_number') != "") {
            $phone_number = $request->get('phone_number');
            $clients = Client::where('phone_number', 'like', "%$phone_number%")
             ->paginate(10);
        } elseif ($request->get('dob_1') 
            && $request->get('dob_2')
            && $request->get('dob_1') != "0") {

            $dob_1 = date("Y-m-d", strtotime($request->get('dob_1')));
            $dob_2 = date("Y-m-d", strtotime($request->get('dob_2')));

            $clients = Client::whereBetween('clients.created_at', array($dob_1, $dob_2))
             ->paginate(10);
        } else {

            $clients = Client::paginate(10);
        }
        return view('clients.index' ,compact('clients', 'links'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientsRequest $request)
    {
        //
        $client = new Client($request->all());
        $client->user_id = Auth::user()->id;
        $client->save();
        return redirect('/clients');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $client = Client::find($id);

        return view('clients.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $client = Client::find($id);
        return view('clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientsRequest $request, $id)
    {
        //
        $client = Client::find($id);
        $client->update(request()->all());

        return redirect('/clients/'.$client->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $client = Client::find($id);
        $client->delete();
        return redirect()->back();
    }

    public function deleteAll(Request $request)
    {   
        $ids = $request->ids;
        Client::whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Clients Deleted successfully."]);
    }
}
