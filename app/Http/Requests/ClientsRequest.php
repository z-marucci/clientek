<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Client;
use Illuminate\Support\Facades\Auth;

class ClientsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        $id = Auth::user()->id;
        
        return [
            //
            'name' => 'required|min:2|max:200',
            'company' => 'min:2|max:200',
            'telephone_number' => 'regex:/^\+?[^a-zA-Z]{5,}$/|max:50',
            'personal_id' => 'sometimes|required|',
            'dob' => 'sometimes|required|date',
            'country' => 'required',
            'email' => 'sometimes|required|max:200|',
            'address' => 'min:10|max:2000',
        ];
    }
}
