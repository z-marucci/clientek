<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Client extends Model
{
    //
    protected $fillable = [
        'name',
        'company',
        'country',
        'phone_number',
        'dob',
        'address',
        'personal_id',
        'email',
        'user_id'
    ];

    public static function formatDob($date)
    {
        $format = 'Y-m-d';
        return Carbon::createFromFormat($format, $date);
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id');
    }
}
