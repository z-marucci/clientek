<?php 
    function get_first_photo(App\client $client)
    {   $firstPhoto = $client->photos->first();
        if (is_null($firstPhoto)) {
            return false;
        }

        return $firstPhoto->thumbnail_path;
    }

    function has_image(App\client $client)
    {
        $firstPhoto = $client->photos->first();
        if (is_null($firstPhoto)) {
            return false;
        }
        return true;
    }

    function get_country_name($code)
    {
        return App\Utilities\Country::getCountryName($code);
    }

    function get_hero_image(App\client $client)
    {   $firstPhoto = $client->photos->first();
        if (is_null($firstPhoto)) {
            return false;
        }

        return $firstPhoto->path;
    }

    function trim_string($title) {
       return mb_strimwidth($title, 0, 50, "...");
    }
?>